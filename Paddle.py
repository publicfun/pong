from turtle import Turtle


class Paddle(Turtle):
    def __init__(self, x_coordinate:int = 0):
        super().__init__()
        self.speed(0)
        self.shape("square")
        self.color("white")
        self.shapesize(stretch_wid=5, stretch_len=1)
        self.penup()
        self.goto(x_coordinate, 0)

    def paddle_up(self):
        y = self.ycor()
        y += 20
        self.sety(y)

    def paddle_down(self):
        y = self.ycor()
        y -= 20
        self.sety(y)


